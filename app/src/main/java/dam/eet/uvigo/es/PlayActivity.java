package dam.eet.uvigo.es;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

public class PlayActivity extends AppCompatActivity {
    // SHARED PREFS
    SharedPreferences sharedPreferences;
    boolean music_settings;
    String graphs_settings;
    String num_asteroids, num_fragments;

    // GAME VIEW
    final double UPDATE_PERIOD = 10; //ms?
    GameView gameView;
    MyPeriodicalTask myPeriodicalTask;

    // MUSIK
    MediaPlayer mp;

    // GEOTHINGS
    final int REQUEST_LOCATION = 14;
    final private int GEOFENCE_RADIUS = 150; // m
    InternalFile f;
    ArrayList<String[]> markersList = new ArrayList<>();
    ArrayList<Geofence> geofenceList = new ArrayList<>();
    GeofencingClient geoClient;
    PendingIntent geoPendingIntent;

    // NOTIFICATIONS
    private String CHANNEL_ID = "materoids";
    private NotificationManagerCompat notif;

    // BROADCAST
    private static final String ACTION_GEOFENCE_TRANSITION = "dam.eet.uvigo.es.ACTION_GEOFENCE_TRANSITION";
    private static final int NON_INTERESTED_TRANSITION = 0x100;
    private final BroadcastReceiver bReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("lab7", "onreceive broadcast");

            int geofenceTransition = intent.getIntExtra("GEOTRANSITION", NON_INTERESTED_TRANSITION);
            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                Log.d("lab7", "broadcast on");
                if (music_settings) {
                    mp.pause();
                }
            } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                Log.d("lab7", "broadcast off");
                if (music_settings) {
                    mp.start();
                }
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        getPrefs();

        // TOAST
        String makeToast = "Music: " + music_settings + "; Graphs: " + graphs_settings +
                "; Asteroids(" + num_asteroids + ") & Fragments(" + num_fragments + ")";
        Toast.makeText(getApplicationContext(), makeToast, Toast.LENGTH_LONG).show();

        // GAME VIEW
        gameView = (GameView) findViewById(R.id.game_view);
        gameView.setFather(this);
        myPeriodicalTask = (MyPeriodicalTask) new MyPeriodicalTask().execute((long) UPDATE_PERIOD);

        // MUSIC
        if (music_settings) {
            mp = MediaPlayer.create(this, R.raw.delfino_plaza);
            //mp.start();
        }

        // GEOTHINGS
        f = new InternalFile(InternalFile.FILE_NAME_MARKERS);
        geoClient = LocationServices.getGeofencingClient(this);
        if (populateGeofenceList()) {
            geoListenersStart();
        }

        // BROADCAST
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_GEOFENCE_TRANSITION);
        this.registerReceiver(bReceiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (music_settings) {
            mp.start();
        }
        //Toast.makeText(getApplicationContext(), "RESUMEMEME", Toast.LENGTH_SHORT).show();
        //Log.d("lifecycle", "onResume: x");
    }

    @Override
    protected void onStop() {
        super.onStop();
        geoListenersStop();
        if (music_settings) {
            mp.pause();
        }
        //Toast.makeText(getApplicationContext(), "STOPOPO", Toast.LENGTH_SHORT).show();
        //Log.d("lifecycle", "onStop: x");
    }

    @Override
    protected void onDestroy() {
        //Toast.makeText(getApplicationContext(), "DESTROYOYO", Toast.LENGTH_SHORT).show();
        //Log.d("lifecycle", "onDestroy: x");

        if (myPeriodicalTask != null) {
            myPeriodicalTask.cancel(true);
        }

        unregisterReceiver(bReceiver);

        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        int pos = 0;
        if (mp != null) {
            pos = mp.getCurrentPosition();
            outState.putInt("position", pos);
        }
        Log.d("lab4", "on save instance pos: " + pos);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int pos = 0;
        if (mp != null) {
            pos = savedInstanceState.getInt("position");
            mp.seekTo(pos);
            Log.d("lab4", "on restore instance pos: " + pos);
        }
    }

    void getPrefs() {
        // private mode by default
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        // second term is the default value
        music_settings = sharedPreferences.getBoolean("music_checkbox", false);
        graphs_settings = sharedPreferences.getString("graphs_checkbox", "1");
        num_asteroids = sharedPreferences.getString("asteroids_num", "0");
        num_fragments = sharedPreferences.getString("fragments_num", "0");
    }

    //periodic tasks
    public class MyPeriodicalTask extends AsyncTask<Long, Void, Boolean> {
        public MyPeriodicalTask() {
        }

        @Override
        // three dots: zero or more Long arguments are going to be passed
        // [arbitrary number of arguments]
        protected Boolean doInBackground(Long... longs) {
            Boolean bool = true;
            while (bool) {
                try {
                    // stops execution for some time (waiting for the asteroids to be updated)
                    Thread.sleep(longs[0].longValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // publish updates on the UI thread (calls OnProgressUpdated)
                publishProgress();
                if (isCancelled()) {
                    bool = false;
                }
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            gameView.update();
        }
    }

    // GEOTHINGS
    public void retrieveMarkersString() {
        String s;
        s = f.loadFile(this.getApplicationContext());

        // split
        if (s != null) {
            String positions[] = s.split("\\r?\\n");
            for (int j = 0; j < positions.length; j++) {
                String[] latlngid = positions[j].split(",");
                markersList.add(latlngid);
                //Log.d("lab7", "lat: " + markersList.get(j)[0] + " & long: " + markersList.get(j)[1]);
            }
        } else {
            markersList.clear();
        }
    }

    public boolean populateGeofenceList() {
        Geofence geofence;
        boolean addedGeofences = false;

        try {
            retrieveMarkersString();

            // if its not a double it'll throw an exception
            Log.d("lab8", "nmber: " + Double.parseDouble(markersList.get(0)[0]));

            if (markersList.size() != 0) {
                Log.d("lab8", "array not empty");
                for (int i = 0; i < markersList.size(); i++) {
                    geofence = new Geofence.Builder().setRequestId(markersList.get(i)[2]).
                            setCircularRegion(Double.parseDouble(markersList.get(i)[0]), Double.parseDouble(markersList.get(i)[1]), GEOFENCE_RADIUS).
                            setExpirationDuration(Geofence.NEVER_EXPIRE).
                            setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT).
                            build();
                    geofenceList.add(geofence);
                    addedGeofences = true;
                }
            } else {
                Log.d("lab8", "array is empty");
                addedGeofences = false;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            addedGeofences = false;
            Log.e("lab7", "excepcion in populate: " + e);
        }
        Log.d("lab7", "n geofences: " + geofenceList.size());
        return addedGeofences;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("lab7", "permission granted");
                geoListenersStart();
            }
        }
    }

    private void geoListenersStart() {
        // checks version and permissions
        boolean aux = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                aux = true;
                Log.d("lab8", "Q or HIGHER");
            }
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                aux = true;
                Log.d("lab8", "Q or LOWER");
            }
        }

        if (aux) {
            geoClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent()).addOnSuccessListener(this, new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d("lab7", "geofences added fine");
                }
            });
            geoClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent()).addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d("lab7", "exception geoListenerStart: " + e);
                }
            });
            return;
        } else {
            Log.d("lab7", "didnt have permissions");
            // ask for permissions
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION}, REQUEST_LOCATION);
        }
    }

    private void geoListenersStop() {
        geoClient.removeGeofences(getGeofencePendingIntent())
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("lab7", "geofences removed fine");
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("lab7", "exception geoListenerStop: " + e);
                    }
                });

    }

    private GeofencingRequest getGeofencingRequest() {
        Log.d("lab7", "georequest");
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        //builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_DWELL); -> user is in the muted are for a minimum time
        builder.addGeofences(geofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // if its already created -> reuse it
        if (geoPendingIntent != null) {
            return geoPendingIntent;
        }

        Intent intent = new Intent(this, GeofenceLocationService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        geoPendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return geoPendingIntent;
    }


}