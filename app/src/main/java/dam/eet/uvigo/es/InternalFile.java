package dam.eet.uvigo.es;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class InternalFile {

    public static final String FILE_NAME_MARKERS = "markers_stored.txt";
    String fileName = "";

    public InternalFile(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String newFileName) {
        this.fileName = newFileName;
    }

    public void saveFile(String text, Context ctx) {
        FileOutputStream f = null;
        try {
            f = ctx.openFileOutput(fileName, Context.MODE_PRIVATE);
            f.write(text.getBytes());
            Toast.makeText(ctx, "File saved to: " + ctx.getFilesDir() + "/" + fileName, Toast.LENGTH_LONG);
            Log.d("file", "file saved to: " + ctx.getFilesDir());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (f != null) {
                try {
                    f.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String loadFile(Context ctx) {
        FileInputStream f = null;
        try {
            f = ctx.openFileInput(fileName);
            InputStreamReader isr = new InputStreamReader(f);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;

            while ((text = br.readLine()) != null) {
                sb.append(text).append("\n");
            }

            Toast.makeText(ctx, "File retrieved from: " + ctx.getFilesDir() + "/" + fileName, Toast.LENGTH_LONG);
            Log.d("file", "file retrieved from: " + ctx.getFilesDir());

            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (f != null) {
                try {
                    f.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
