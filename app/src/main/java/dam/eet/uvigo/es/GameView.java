package dam.eet.uvigo.es;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

@TargetApi(21)

public class GameView extends View implements SensorEventListener {
    // CONSTANTS
    final double HORIZONTAL_MOVEMENT = 5;
    final double VERTICAL_MOVEMENT = 10;
    final double SPACESHIP_ROTATION_CHANGE = 0.1;
    final double SPACESHIP_SPEED_CHANGE = 8;
    final int MISSILE_SPEED_STEP = 4;
    final double ASTEROIDS_SPEED_FACTOR = 4; //s
    final double SPACESHIP_SPEED_FACTOR = 5; //s
    final double MISSILE_SPEED = 5; //s
    final int ACTIVE_PERIODS = 40; //ACTIVE_PERIOD*UPDATE_PERIOD=ACTIVE_TIME
    final int GYRO_STEP = -2;
    final float ACC_STEP = (float) 0.3;


    // asteroids
    SharedPreferences sharedPreferences;
    String num_asteroids, num_fragments;
    final String _defaultNumber = "4";
    Drawable[] drawableAsteroid = new Drawable[3];    //array
    Vector<Graph> Asteroids;

    // spaceship
    Graph spaceship;
    Drawable[] drawableSpaceship = new Drawable[2];    //array
    double spaceship_rotation = 45;
    double spaceship_speed = 1;

    // missiles
    Vector<Graph> Missiles;
    Drawable[] drawableMissile = new Drawable[3];
    boolean missile_ON = false;
    int destroyed_asteroids = 0;
    int total_asteroids = 0;

    // finish game
    Graph finish_duck;
    Graph win_duck;
    Drawable[] drawableFinish = new Drawable[2];

    // aux variables
    Date date = new Date();
    double x_rand, y_rand = 0;
    float mX, mY = 0;

    // multimedia
    SoundPool sp;
    boolean music_settings;
    int id_fire, id_explosion, id_endgame, id_wongame;
    private static final int MAX_NUMBER_SIMULTANEOUS_REPRODUCTIONS = 5;
    private static final float LEFT_CHANNEL_VOLUME = 1.0f;
    private static final float RIGHT_CHANNEL_VOLUME = 1.0f;
    private static final int NUMBER_OF_REPLIES = 0;
    private static final float PITCH = 1f;

    // scores
    Activity father;

    // movement sensors
    private static final int SHAKE_THRESHOLD = 5000;
    boolean sensors_on;
    boolean has_acc;
    SensorManager sensorManager;
    Sensor acc, gyro;
    float last_x = 0;
    float last_y = 0;
    float last_z = 0;
    long last_update;

    // easter egg
    boolean rainbow_mode;
    private final int EGG_TIMES = 25;
    int i_egg = 0;
    Graph egg;
    Drawable[] drawableEgg = new Drawable[1];


    // constructor
    //@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public GameView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        // shared preferences
        getPrefs(context);
        total_asteroids = Integer.parseInt(num_asteroids);

        // check movement sensors
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        // list sensors
        List<Sensor> deviceSensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
        /*
        for (int i=0; i< deviceSensors.size(); i++){
            Log.d("lab5", "sensor: " + deviceSensors.get(i));
        }
        */
        if (sensors_on && sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            Log.d("lab5", "sensors on");
            acc = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, acc, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (sensors_on && sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
            Log.d("lab5", "sensors on");
            gyro = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            sensorManager.registerListener(this, gyro, SensorManager.SENSOR_DELAY_NORMAL);
        }

        // get asteroids
        drawableAsteroid[0] = context.getResources().getDrawable(R.drawable.asteroide1, null);
        drawableAsteroid[1] = context.getResources().getDrawable(R.drawable.asteroide2, null);
        drawableAsteroid[2] = context.getResources().getDrawable(R.drawable.asteroide3, null);

        // create n asteroids with speed V=i*incx +j*incy and rotation angle
        Asteroids = new Vector<Graph>();
        for (int i = 0; i < Integer.parseInt(num_asteroids); i++) {
            // only small asteroids for the moment
            Graph asteroid = new Graph(this, drawableAsteroid[0], true);
            // random speeds
            asteroid.setIncX(Math.random() * 4 - 2);
            asteroid.setIncY(Math.random() * 4 - 2);
            asteroid.setRotationAngle((int) (Math.random() * 8 - 4));
            Asteroids.add(asteroid);
        }

        // get spaceships
        drawableSpaceship[0] = context.getResources().getDrawable(R.drawable.nave, null);
        drawableSpaceship[1] = context.getResources().getDrawable(R.drawable.nave_fuego, null);
        spaceship = new Graph(this, drawableSpaceship[0], true);

        // missiles
        Missiles = new Vector<Graph>();
        if (rainbow_mode) {
            drawableMissile[0] = context.getResources().getDrawable(R.drawable.rubber_duck_big, null);
        } else {
            drawableMissile[0] = context.getResources().getDrawable(R.drawable.rubber_duck2, null);
        }
        //drawableMissile[1] = context.getResources().getDrawable(R.drawable.misil2, null);
        //drawableMissile[2] = context.getResources().getDrawable(R.drawable.misil3, null);

        // finish game
        drawableFinish[0] = context.getResources().getDrawable(R.drawable.end_duck, null);
        drawableFinish[1] = context.getResources().getDrawable(R.drawable.win_duck, null);
        finish_duck = new Graph(this, drawableFinish[0], false);
        win_duck = new Graph(this, drawableFinish[1], false);

        // egg
        drawableEgg[0] = context.getResources().getDrawable(R.drawable.megasuperchevere);
        egg = new Graph(this, drawableEgg[0], false);

        // multimedia
        SoundPool.Builder spb = new SoundPool.Builder();
        spb.setMaxStreams(MAX_NUMBER_SIMULTANEOUS_REPRODUCTIONS);
        sp = spb.build();
        id_explosion = sp.load(this.getContext(), R.raw.windows, 0);
        id_fire = sp.load(this.getContext(), R.raw.duck, 0);
        id_endgame = sp.load(this.getContext(), R.raw.endgame, 0);
    }

    void getPrefs(Context context) {
        // private mode by default
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        // second term is the default value
        num_asteroids = sharedPreferences.getString("asteroids_num", _defaultNumber);
        num_fragments = sharedPreferences.getString("fragments_num", _defaultNumber);
        sensors_on = sharedPreferences.getBoolean("sensors_checkbox", false);
        music_settings = sharedPreferences.getBoolean("music_checkbox", false);
        rainbow_mode = sharedPreferences.getBoolean("rainbow_mode", false);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // each Graph in the vector Asteroids (receiving the name 'x')
        for (Graph x : Asteroids) {
            // avoiding left corner
            x_rand = 0;
            y_rand = 0;
            while (x_rand < 200 && y_rand < 100) {
                x_rand = Math.random() * (w - x.getWidth());
                y_rand = Math.random() * (h - x.getHeight());
            }
            x.setPosX(x_rand);
            x.setPosY(y_rand);
        }
        // locate spaceship (initially)
        // spaceship.setGraph_on(true);
        spaceship.setPosX(0);
        spaceship.setPosY(0);
        spaceship.setIncX(spaceship_speed * Math.cos(Math.toRadians(spaceship.getRotationAngle())));
        spaceship.setIncY(spaceship_speed * Math.cos(Math.toRadians(spaceship.getRotationAngle())));
        spaceship.setRotationAngle(spaceship_rotation);

        // finish message
        finish_duck.setPosX((w / 2) - (finish_duck.getWidth() / 2));
        finish_duck.setPosY((h / 2) - (finish_duck.getHeight() / 2));
        win_duck.setPosX((w / 2) - (finish_duck.getWidth() / 2));
        win_duck.setPosY((h / 2) - (finish_duck.getHeight() / 2));

        // egg
        egg.setPosX((w / 2) - (finish_duck.getWidth() / 2));
        egg.setPosY((h / 2) - (finish_duck.getHeight() / 2));

        // Log.d("lab4", "onSizeChanged");
    }

    @Override
    // bein' constantly called
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (spaceship.getGraphON()) {
            spaceship.drawGraph(canvas);
        }

        for (Graph x : Asteroids) {
            x.drawGraph(canvas);
        }

        for (Graph x : Missiles) {
            x.drawGraph(canvas);
        }

        if (finish_duck.getGraphON()) {
            finish_duck.drawGraph(canvas);
        }
        if (win_duck.getGraphON()) {
            win_duck.drawGraph(canvas);
        }

        if (egg.getGraphON()) {
            if (i_egg == EGG_TIMES) {
                egg.setGraphON(false);
            } else {
                egg.drawGraph(canvas);
                i_egg++;
            }
        }
    }

    // updating positions!
    protected synchronized void update() {
        //Log.d("lab4","update");
        for (Graph x : Asteroids) {
            x.updatePos(ASTEROIDS_SPEED_FACTOR);
        }
        // don't collide!
        for (int j = 0; j < Asteroids.size(); j++) {
            if (Asteroids.elementAt(j).verifyCollision(spaceship)) {
                Log.d("lab4", "spaceship defeated!");
                finishGame();
            }
        }

        spaceship.setRotationAngle(spaceship_rotation);
        spaceship.setIncX(spaceship_speed * Math.cos(Math.toRadians(spaceship.getRotationAngle())));
        spaceship.setIncY(spaceship_speed * Math.sin(Math.toRadians(spaceship.getRotationAngle())));
        spaceship.updatePos(SPACESHIP_SPEED_FACTOR);

        List<Graph> newFragments = new ArrayList<>();
        List<Graph> byebyeMissiles = new ArrayList<>();
        List<Graph> byebyeAsteroids = new ArrayList<>();

        // why not: Missiles.getIndexof() ¿? :'(
        for (int i = 0; i < Missiles.size(); i++) {
            Missiles.elementAt(i).updatePos(MISSILE_SPEED);
            // check timeout
            Missiles.elementAt(i).updateActiveTime();
            if (Missiles.elementAt(i).getActiveTime() == 0) {
                Log.d("lab4", "timeout missile");
                destroyMissile(i);
            } else {
                // check collision
                for (int j = 0; j < Asteroids.size(); j++) {
                    if (Asteroids.elementAt(j).verifyCollision(Missiles.elementAt(i))) {
                        if (music_settings) {
                            sp.play(id_explosion, LEFT_CHANNEL_VOLUME, RIGHT_CHANNEL_VOLUME, 1, NUMBER_OF_REPLIES, PITCH);
                        }
                        byebyeMissiles.add(Missiles.elementAt(i));

                        if (Asteroids.elementAt(j).getDrawable() == drawableAsteroid[0]) {
                            total_asteroids += Integer.parseInt(num_fragments);
                            destroyed_asteroids++;
                            Log.d("lab8", "total: " +  total_asteroids);

                            for (int k = 0; k < Integer.parseInt(num_fragments); k++) {
                                // only small asteroids for the moment
                                Graph fragment = new Graph(this, drawableAsteroid[2], true);
                                // random speeds
                                fragment.setPosX(Asteroids.elementAt(j).getPosX());
                                fragment.setPosY(Asteroids.elementAt(j).getPosY());
                                fragment.setIncX(Math.random() * 4 - 2);
                                fragment.setIncY(Math.random() * 4 - 2);
                                fragment.setRotationAngle((int) (Math.random() * 8 - 4));
                                newFragments.add(fragment);
                            }
                        } else if (Asteroids.elementAt(j).getDrawable() == drawableAsteroid[2]) {
                            destroyed_asteroids++;
                        }
                        byebyeAsteroids.add(Asteroids.elementAt(j));
                    }
                }

                // destroy everything!
                Missiles.removeAll(byebyeMissiles);
                Asteroids.removeAll(byebyeAsteroids);
                Asteroids.addAll(newFragments);

                if (Asteroids.size() == 0) {
                    // no asteroids left
                    finishGame();
                }
            }

                /*
                        if (x.getDrawable() == drawableAsteroid[0]) {
                            //x.setDrawable(drawableAsteroid[1]);
                            Log.d("lab5", "drawable 0");
                        } else if (x.getDrawable() == drawableAsteroid[1]) {
                            //x.setDrawable(drawableAsteroid[2]);
                            Log.d("lab5", "drawable 1");
                        } else if (x.getDrawable() == drawableAsteroid[2]) {
                            //destroyAsteroid(j);
                            Log.d("lab5", "drawable 2");
                            destroyed_asteroids++;
                        }
                        */
        }
    }

    private void destroyAsteroid(int pos) {
        Asteroids.remove(pos);
    }

    private void destroyMissile(int pos) {
        Missiles.remove(pos);
    }

    private void activatedMissile() {
        Graph missile = new Graph(this, drawableMissile[0], true);
        missile.setPosX(spaceship.getPosX() + spaceship.getWidth() / 2 - missile.getWidth() / 2);
        missile.setPosY(spaceship.getPosY() + spaceship.getHeight() / 2 - missile.getHeight() / 2);
        missile.setRotationAngle(spaceship.getRotationAngle());
        missile.setRotationSpeed(0);
        missile.setIncX(Math.cos(Math.toRadians(missile.getRotationAngle())) * MISSILE_SPEED_STEP);
        missile.setIncY(Math.sin(Math.toRadians(missile.getRotationAngle())) * MISSILE_SPEED_STEP);
        //missile.setActiveTime(((int) Math.min(this.getWidth() / Math.abs(missile.getIncX()), this.getHeight() / Math.abs(missile.getIncY()))) + date.getTime());
        missile.setActiveTime(40); //40*UPDATE_PERIOD;
        Missiles.add(missile);
        //music
        if (music_settings) {
            sp.play(id_fire, LEFT_CHANNEL_VOLUME, RIGHT_CHANNEL_VOLUME, 1, NUMBER_OF_REPLIES, PITCH);
        }
    }

    // finish the game
    private void finishGame() {
        Log.d("lab8", "total: " + total_asteroids + " & destroyed: " + destroyed_asteroids);
        // calculating score
        float mateo = ((float) destroyed_asteroids / (float) (total_asteroids)) * 100;
        int score = (int) mateo;

        // play some musik and show image
        if (mateo == 100) {
            if (music_settings) {
                sp.play(id_wongame, LEFT_CHANNEL_VOLUME, RIGHT_CHANNEL_VOLUME, 1, NUMBER_OF_REPLIES, PITCH);
            }
            win_duck.setGraphON(true);
        } else {
            if (music_settings) {
                sp.play(id_wongame, LEFT_CHANNEL_VOLUME, RIGHT_CHANNEL_VOLUME, 1, NUMBER_OF_REPLIES, PITCH);
            }
            finish_duck.setGraphON(true);
        }

        Bundle bundle = new Bundle();
        bundle.putInt("score", score);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        father.setResult(Activity.RESULT_OK, intent);
        father.finish();
    }

    public void setFather(Activity father) {
        this.father = father;
    }

    // TOUCH SCREEN
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);

        // where did the touch happened
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            // A pressed gesture has started, the motion contains the initial starting location.
            case MotionEvent.ACTION_DOWN:
                Log.d("lab4", "ACTION DOWN");
                missile_ON = true;
                break;
            // between ACTION_DOWN and ACTION_UP
            case MotionEvent.ACTION_MOVE:
                float dx = Math.abs((x - mX));
                float dy = Math.abs((y - mY));

                try {
                    if (dy < VERTICAL_MOVEMENT && dx > HORIZONTAL_MOVEMENT) {
                        spaceship_rotation = Math.round(x - mX) / SPACESHIP_ROTATION_CHANGE;
                        missile_ON = false;
                        Log.d("lab4", "spaceship rotation: " + spaceship_rotation);
                    } else if (dy > VERTICAL_MOVEMENT && dx < HORIZONTAL_MOVEMENT) {
                        spaceship_speed = Math.round(mY - y) / SPACESHIP_SPEED_CHANGE;
                        Log.d("lab4", "spaceship speed: " + spaceship_speed);
                        missile_ON = false;
                    }
                } catch (Error e) {
                    Log.d("lab4", e.toString());
                    Log.d("lab4", "smthing bad");
                }

                break;
            case MotionEvent.ACTION_UP:
                Log.d("lab4", "ACTION UP");
                //spaceship_speed = 0;
                //spaceship_rotation = 0;
                if (missile_ON) {
                    Log.d("lab4", "missile launched");
                    activatedMissile();
                }
                break;
        }
        mX = x;
        mY = y;
        return true;
    }

    // MOVEMENTS
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            long curTime = System.currentTimeMillis();
            // only allow one update every 100ms.
            if ((curTime - last_update) > 100) {
                long diffTime = (curTime - last_update);
                last_update = curTime;

                float acc_x = event.values[0];
                float acc_y = event.values[1];
                float acc_z = event.values[2];


                // controlling speed
                spaceship_speed = acc_y * ACC_STEP;

                // shaky shaky
                float speed = Math.abs(acc_x + acc_y + acc_z - last_x - last_y - last_z) / diffTime * 10000;

                // SHAKE DETECTED
                if (speed > SHAKE_THRESHOLD) {
                    // Log.d("lab5", "shake detected w/ speed: " + speed);
                    superMegaSecretAmazingWonderfulEasterEgg();
                }
                last_x = acc_x;
                last_y = acc_y;
                last_z = acc_z;
            }
        }
        if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            //float axisX = event.values[0];
            //float axisY = event.values[1];
            float axisZ = event.values[2];

            spaceship_rotation = Math.toDegrees(GYRO_STEP * axisZ);
        }

    }

    private void superMegaSecretAmazingWonderfulEasterEgg() {
        i_egg = 0;
        egg.setGraphON(true);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // nothing 4 the moment
    }
}


