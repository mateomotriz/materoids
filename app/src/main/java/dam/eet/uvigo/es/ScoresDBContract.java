package dam.eet.uvigo.es;

import android.provider.BaseColumns;

public class ScoresDBContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private ScoresDBContract() {
    }

    public static class ScoresEntry implements BaseColumns {
        public static final String TABLE_NAME = "scores";
        public static final String COLUMN_NAME_TIME = "timestamp";
        public static final String COLUMN_NAME_PLAYER = "player";
        public static final String COLUMN_NAME_SCORE = "score";
    }
}
