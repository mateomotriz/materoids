package dam.eet.uvigo.es;

public class ScoreInfo {
    private String player_name;
    private int timestamp, score;

    public ScoreInfo(String playerName, int score, int timestamp) {
        this.player_name = playerName;
        this.score = score;
        this.timestamp = timestamp;
    }

    public String getPlayerName() {
        return this.player_name;
    }

    public void setPlayerName(String name) {
        this.player_name = name;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }
}
