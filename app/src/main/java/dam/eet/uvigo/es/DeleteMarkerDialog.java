package dam.eet.uvigo.es;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class DeleteMarkerDialog extends DialogFragment implements View.OnClickListener {
    private EditText txt;

    public interface DeleteMarkerListener {
        void onFinishDeleteMarkerDialog(String markerID);
    }

    public DeleteMarkerDialog() {
        // empty constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("lab6", "delete dialog");
        View view = inflater.inflate(R.layout.dialog_marker, null, false);
        getDialog().setTitle("Delete marker");

        // buttons
        Button delete_button = view.findViewById(R.id.delete_button);
        delete_button.setOnClickListener(this);
        Button cancel_button = view.findViewById(R.id.cancel_button);
        cancel_button.setOnClickListener(this);

        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.delete_button:
                DeleteMarkerListener dml = (DeleteMarkerListener) getActivity();
                dml.onFinishDeleteMarkerDialog((String) this.getArguments().get("marker_id"));

                Log.d("lab6", "delete button clicked");
                break;
            case R.id.cancel_button:
                Log.d("lab6", "cancel button clicked");
                break;
            default:
                Log.d("lab6", "PROBLEMO");
        }
        this.dismiss();
    }
}
