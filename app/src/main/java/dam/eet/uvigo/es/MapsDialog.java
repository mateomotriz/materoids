package dam.eet.uvigo.es;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class MapsDialog extends DialogFragment implements TextView.OnEditorActionListener {
    private EditText txt;

    public interface MapsDialogListener {
        void onFinishMapsDialog(String markerID, String inputText);
    }

    public MapsDialog() {
        // empty constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("lab6", "create view dialog");
        View view = inflater.inflate(R.layout.dialog_maps, null, false);
        txt = (EditText) view.findViewById(R.id.edit_marker);
        getDialog().setTitle("Mute place");

        // Show soft keyboard automatically
        txt.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        txt.setOnEditorActionListener(this);

        return view;
    }

    // Interface definition for a callback to be invoked when an action is performed on the editor.
    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (EditorInfo.IME_ACTION_DONE == i) {
            MapsDialogListener mdl = (MapsDialogListener) getActivity();
            mdl.onFinishMapsDialog((String) this.getArguments().get("marker_id"), txt.getText().toString());
            this.dismiss();
            return true;
        }

        return false;
    }
}
