package dam.eet.uvigo.es;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class ScoreDBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Asteroids.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + ScoresDBContract.ScoresEntry.TABLE_NAME + " (" +
                    ScoresDBContract.ScoresEntry._ID + " INTEGER PRIMARY Key," +
                    ScoresDBContract.ScoresEntry.COLUMN_NAME_TIME + " INTEGER," +
                    ScoresDBContract.ScoresEntry.COLUMN_NAME_PLAYER + " TEXT," +
                    ScoresDBContract.ScoresEntry.COLUMN_NAME_SCORE + " INTEGER)";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + ScoresDBContract.ScoresEntry.TABLE_NAME;

    // constructor
    public ScoreDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onDowngrade(db, oldVersion, newVersion);
    }
}
