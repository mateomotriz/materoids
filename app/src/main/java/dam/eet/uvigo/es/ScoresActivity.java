package dam.eet.uvigo.es;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.ListFragment;

import android.app.Activity;
import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.internal.constants.ListAppsActivityContract;

import java.util.ArrayList;
import java.util.List;

import static dam.eet.uvigo.es.ScoreContentProvider.CONTENT_URI;
import static dam.eet.uvigo.es.ScoresDBContract.ScoresEntry.COLUMN_NAME_PLAYER;
import static dam.eet.uvigo.es.ScoresDBContract.ScoresEntry.COLUMN_NAME_SCORE;
import static dam.eet.uvigo.es.ScoresDBContract.ScoresEntry.COLUMN_NAME_TIME;
import static dam.eet.uvigo.es.ScoresDBContract.ScoresEntry.TABLE_NAME;

public class ScoresActivity extends ListActivity {
    ContentResolver contentResolver;
    Cursor cursor;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scores);

        // must-have 2 access the database
        contentResolver = getContentResolver();

        // first we clean the table
        Log.d("lab5", "delete table");
        //contentResolver.delete(CONTENT_URI, null, null);

        List<ScoreInfo> list = new ArrayList<ScoreInfo>();

        cursor = contentResolver.query(CONTENT_URI, null, null, null, null);

        while (cursor.moveToNext()) {
            ScoreInfo score = new ScoreInfo(null, 0, 0);
            score.setTimestamp(cursor.getInt(1));
            score.setPlayerName(cursor.getString(2));
            score.setScore(cursor.getInt(3));

            list.add(score);
            Log.d("lab3", "name: " + score.getPlayerName());
            Log.d("lab3", "timestamp: " + score.getTimestamp());
            Log.d("lab3", "score: " + score.getScore());
        }
        // Log.d("lab3", "list length: " + list.size());
        setListAdapter(new ScoresAdapter(this, list));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    // menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("lab5", "menu scores!");
        getMenuInflater().inflate(R.menu.menu_scores, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Log.d("lab5", "on opts item selected");
        int id = item.getItemId();
        Log.d("lab5", "id: " + id + " and menu: " + R.id.menu_share);
        if (id == R.id.menu_share) {
            startActivity(getDefaultIntent());
        }
        return super.onOptionsItemSelected(item);
    }

    private Intent getDefaultIntent() {
        Log.d("lab5", "default intent");
        String shared_text;
        ScoreInfo score;

        Intent intent = new Intent(Intent.ACTION_SEND);
        if (getListAdapter().getCount() == 0) {
            shared_text = "kein";
        } else {
            shared_text = getString(R.string.score_title) + " \n";
            int num_scores = getListAdapter().getCount();
            for (int i = 0; i < num_scores; i++) {
                score = (ScoreInfo) getListAdapter().getItem(i);
                shared_text += score.getPlayerName() + " " + score.getScore() + " \n";
            }
        }

        intent.putExtra(Intent.EXTRA_TEXT, shared_text);
        intent.setType("text/plain");
        return intent;
    }
}