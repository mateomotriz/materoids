package dam.eet.uvigo.es;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Parcelable;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

public class GeofenceLocationService extends IntentService {
    // NOTIFICATIONS
    private String CHANNEL_ID = "materoids";
    private NotificationManagerCompat notif;

    // GEOFENCE
    private static final String ACTION_GEOFENCE_TRANSITION = "dam.eet.uvigo.es.ACTION_GEOFENCE_TRANSITION";

    public GeofenceLocationService() {
        super("a su servicio :)");
    }

    @Override
    public void onCreate() {
        // NOTIFICATIONS
        createNotificationChannel();
        notif = NotificationManagerCompat.from(this);
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        GeofencingEvent geoEvent = GeofencingEvent.fromIntent(intent);

        if (geoEvent.hasError()) {
            Log.d("lab7", "some error handling");
        }

        int geoTransition = geoEvent.getGeofenceTransition();
        if (geoTransition == Geofence.GEOFENCE_TRANSITION_ENTER || geoTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            List<Geofence> trigGeofences = geoEvent.getTriggeringGeofences();
            // hehe
            for (int i = 0; i < trigGeofences.size(); i++) {
                Log.d("lab7", "geofence: " + trigGeofences.get(i).getRequestId());
            }

            // send broadcast
            Intent filter = new Intent();
            filter.putExtra("GEOTRANSITION", geoTransition);
            filter.setAction(ACTION_GEOFENCE_TRANSITION);
            sendBroadcast(filter);

            // notifs
            if (geoTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
                sendNotification("Music muted! You're inside of one of your geofences!");
            } else if (geoTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
                sendNotification("Music on! You're outside of your geofences!");
            }
        }
    }

    // NOTIFS
    private void createNotificationChannel() {
        String channel_name = "materoids";
        String channel_description = "4 >= Android Oreo";

        Log.d("lab7", "notif channel created");

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, channel_name, importance);
            channel.setDescription(channel_description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            // customize
            // channel.enableLights(true);
            // channel.setLightColor(R.color.Aquamarine);
        }
    }

    private void sendNotification(String message) {
        // if w hit the notif and then 'back', we'll continue where we were before
        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // send notif
        String title = String.valueOf((R.string.app_name));
        Notification n = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.rubber_duck2)
                .setContentTitle("Materoids")
                .setContentText(message)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
                .build();

        notif.notify(1, n);
    }
}
