package dam.eet.uvigo.es;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

public class Graph {
    private Drawable drawable;
    private double posX, posY;      // Position
    private double incX, incY;      // Moving speed
    private double rotationAngle;   // Rotation angle
    private double rotationSpeed;   // Rotation speed
    private int width, height;      // Dimensions
    private int collisionRadio;     // Indicate when objects collide
    private View view;
    public static final int MAX_SPEED = 20;

    // destroy graph!
    private boolean graph_on;
    private int active_time;
    private long timestamp;

    public static int getMaxSpeed() {
        return MAX_SPEED;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public double getPosX() {
        return posX;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public double getPosY() {
        return posY;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }

    public double getIncX() {
        return incX;
    }

    public void setIncX(double incX) {
        this.incX = incX;
    }

    public double getIncY() {
        return incY;
    }

    public void setIncY(double incY) {
        this.incY = incY;
    }

    public double getRotationAngle() {
        return rotationAngle;
    }

    public void setRotationAngle(double rotationAngle) {
        this.rotationAngle = rotationAngle;
    }

    public double getRotationSpeed() {
        return rotationSpeed;
    }

    public void setRotationSpeed(double rotationSpeed) {
        this.rotationSpeed = rotationSpeed;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getCollisionRadio() {
        return collisionRadio;
    }

    public void setCollisionRadio(int collisionRadio) {
        this.collisionRadio = collisionRadio;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    /* -------------------------------*/
    public boolean getGraphON(){
        return this.graph_on;
        //return missile_on;
    }

    public void setGraphON(boolean on ){
        this.graph_on = on;
    }

    public int getActiveTime(){
        return this.active_time;
    }

    public void setActiveTime(int time){
        this.active_time = time;
    }

    public void updateActiveTime(){
        this.active_time -= 1;
        //Log.d("lab4","active time: " + active_time);
    }

    /* -------------------------------*/

    public Graph(View view, Drawable drawable, boolean graph_on) {
        this.view = view;
        this.drawable = drawable;
        this.graph_on = graph_on;

        width = drawable.getIntrinsicWidth();
        height = drawable.getIntrinsicHeight();
        collisionRadio = (width + height) / 4;
    }

    public void drawGraph(Canvas canvas) {
        canvas.save();
        int x = (int) (posX + width / 2);
        int y = (int) (posY + height / 2);
        canvas.rotate((float) rotationAngle, (float) x, (float) y);
        drawable.setBounds((int) posX, (int) posY, (int) posX + width, (int) (posY + height));
        drawable.draw(canvas);
        canvas.restore();
        int rInval = (int) Math.hypot(width, height) / 2 + MAX_SPEED;
        view.invalidate(x - rInval, y - rInval, x + rInval, y + rInval);
    }

    public void updatePos(double factor) {
        posX += incX * factor;
        if (posX < -width / 2) {
            posX = view.getWidth() - width / 2;
        }
        if (posX > view.getWidth() - width / 2) {
            posX = -width / 2;
        }
        posY += incY * factor;
        if (posY < -height / 2) {
            posY = view.getHeight() - height / 2;
        }
        if (posY > view.getHeight() - height / 2) {
            posY = -height / 2;
        }
        rotationAngle += rotationSpeed * factor; // Angle update
    }

    public double distance(Graph g) {
        return Math.hypot(posX - g.posX, posY - g.posY);
    }

    public boolean verifyCollision(Graph g) {
        return (distance(g) < (collisionRadio + g.collisionRadio));
    }
}

