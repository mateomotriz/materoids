package dam.eet.uvigo.es;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import static dam.eet.uvigo.es.ScoreContentProvider.CONTENT_URI;
import static dam.eet.uvigo.es.ScoresDBContract.ScoresEntry.COLUMN_NAME_PLAYER;
import static dam.eet.uvigo.es.ScoresDBContract.ScoresEntry.COLUMN_NAME_SCORE;
import static dam.eet.uvigo.es.ScoresDBContract.ScoresEntry.COLUMN_NAME_TIME;
import static java.lang.System.currentTimeMillis;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    // scores
    int GAME_REQUEST_CODE;

    // shared prefs
    SharedPreferences sharedPreferences;
    String player_name;

    // db
    ContentResolver contentResolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Buttons
        Button about_button = findViewById(R.id.about_button);
        about_button.setOnClickListener(this);

        Button exit_button = findViewById(R.id.exit_button);
        exit_button.setOnClickListener(this);

        Button setup_button = findViewById(R.id.setup_button);
        setup_button.setOnClickListener(this);

        Button play_button = findViewById(R.id.play_button);
        play_button.setOnClickListener(this);

        Button score_button = findViewById(R.id.score_button);
        score_button.setOnClickListener(this);

        Button maps_button = findViewById(R.id.maps_button);
        maps_button.setOnClickListener(this);

        // db
        contentResolver = getContentResolver();
    }

    // menu!
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("lab5", "menu main!");
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == R.id.settings_menu) {
            initSettings(null);
        }
        if (item_id == R.id.about_menu) {
            initAbout(null);
        }
        //item_id == R.id.about_menu ? initAbout(null) : null;

        return super.onOptionsItemSelected(item);
    }

    // manage buttons
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.about_button:
                Toast.makeText(getApplicationContext(), R.string.the_artist, Toast.LENGTH_SHORT).show();
                initAbout(null);
                break;
            case R.id.exit_button:
                Toast.makeText(getApplicationContext(), R.string.exit_button_info, Toast.LENGTH_SHORT).show();
                finish();
                break;
            case R.id.setup_button:
                Toast.makeText(getApplicationContext(), R.string.exit_button_info, Toast.LENGTH_SHORT).show();
                initSettings(null);
                break;
            case R.id.play_button:
                initPlay(null);
                break;
            case R.id.score_button:
                initScore(null);
            case R.id.maps_button:
                initMaps(null);
        }
    }

    public void initAbout(View view) {
        Intent i = new Intent(this, AboutActivity.class);
        startActivity(i);
    }

    public void initSettings(View view) {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    public void initPlay(View view) {
        Intent i = new Intent(this, PlayActivity.class);
        startActivityForResult(i, GAME_REQUEST_CODE);
    }

    public void initScore(View view) {
        Intent i = new Intent(this, ScoresActivity.class);
        startActivity(i);
    }

    public void initMaps(View view) {
        Intent i = new Intent(this, MapsActivity.class);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        getPrefs();
        if (requestCode == GAME_REQUEST_CODE & resultCode == RESULT_OK & data != null) {
            int score = data.getExtras().getInt("score");
            // pending: adding shared preferences
            ContentValues cv = new ContentValues();
            // TABLE -> id / time / player / score
            cv.put(COLUMN_NAME_TIME, (int) System.currentTimeMillis());
            cv.put(COLUMN_NAME_PLAYER, (String) player_name);
            cv.put(COLUMN_NAME_SCORE, (int) score);
            Uri rowUri = contentResolver.insert(CONTENT_URI, cv);
            // init scores activity
            initScore(null);
        }
    }

    void getPrefs() {
        // private mode by default
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        // second term is the default value
        player_name = sharedPreferences.getString("player_name", "¿?");
    }
}

