package dam.eet.uvigo.es;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ScoresAdapter extends BaseAdapter {
    private final Activity activity;
    private final List<ScoreInfo> list;

    public ScoresAdapter(Activity activity, List<ScoreInfo> list) {
        this.activity = activity;
        this.list = list;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        int score;
        String player;
        int imageID;

        Log.d("lab3", "getView:");

        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.scorelist_element, null, false);

        score = list.get(position).getScore();
        player = list.get(position).getPlayerName();
        // here i should implement getting 3 different asteroid sizes depending on the score
        //imageID = activity.getResources().getDrawable(R.drawable.asteroide1); -> alternative
        if (score < 50){
            imageID = R.drawable.asteroide3;
        }
        else if (score >= 50 && score <= 90){
            imageID = R.drawable.asteroide2;
        }
        else{
            imageID = R.drawable.asteroide1;
        }

        TextView scoreText = view.findViewById(R.id.score);
        scoreText.setText(Integer.toString(score));

        TextView playerText = view.findViewById(R.id.player_name);
        playerText.setText(player);

        ImageView imageView = view.findViewById(R.id.image_score);
        //imageView.setImageDrawable(imageID); -> alternative
        imageView.setImageResource(imageID);

        return view;
    }

    public int getCount() {
        try {
            return list.size();
        } catch (NullPointerException e) {
            Log.e("ScoresAdapter", "empty list");
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return (long) i;
    }
}
