package dam.eet.uvigo.es;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.Projection;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import static android.content.ContentUris.withAppendedId;
import static android.provider.BaseColumns._ID;
import static dam.eet.uvigo.es.ScoresDBContract.ScoresEntry.COLUMN_NAME_PLAYER;
import static dam.eet.uvigo.es.ScoresDBContract.ScoresEntry.COLUMN_NAME_SCORE;
import static dam.eet.uvigo.es.ScoresDBContract.ScoresEntry.COLUMN_NAME_TIME;
import static dam.eet.uvigo.es.ScoresDBContract.ScoresEntry.TABLE_NAME;

// UPDATE MANIFEST!
public class ScoreContentProvider extends ContentProvider {
    private static final String AUTHORITY = "dam.eet.uvigo.es.asteroids2021";
    private static final String BASE_PATH = "scores";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
    private static final int SCORES = 1;
    private static final int SCORES_ID = 2;
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    // we are using all columns
    final static String[] PROJECTION = {
            _ID,
            COLUMN_NAME_TIME,
            COLUMN_NAME_PLAYER,
            COLUMN_NAME_SCORE,
    };

    static {
        // Sets the integer value for multiple rows in table 3 to 1. Notice that no wildcard is used in the path
        uriMatcher.addURI(AUTHORITY, BASE_PATH, SCORES);
        // Sets the code for a single row to 2
        uriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", SCORES_ID);
    }

    private SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        ScoreDBHelper scoreDBHelper = new ScoreDBHelper(getContext());
        db = scoreDBHelper.getWritableDatabase();
        return true;
    }

    @Nullable
    @Override
    // request from clients
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        // How do you want the results sorted in the resulting Cursor
        String sortOrder = COLUMN_NAME_SCORE + " DESC";
        String selection = null;
        Cursor cursor;
        /*
         * Choose the table to query and a sort order based on the code returned for the incoming
         * URI. Here, too, only the statements for table 3 are shown.
         */
        switch (uriMatcher.match(uri)) {
            // If the incoming URI was for all of the table
            case 1:
                cursor = db.query(TABLE_NAME, PROJECTION, null, null, null, null, sortOrder);
                break;
            // If the incoming URI was for a single row
            case 2:
                /*
                 * Because this URI was for a single row, the _ID value part is
                 * present. Get the last path segment from the URI; this is the _ID value.
                 * Then, append the value to the WHERE clause for the query
                 */
                selection = selection + "_ID = " + uri.getLastPathSegment();
                cursor = db.query(TABLE_NAME, PROJECTION, selection, null, null, null, sortOrder);
                break;

            default:
                throw new Error("Problem w UriMatcher");
                // If the URI is not recognized, you should do some error handling here.
        }
        // call the code to actually do the query


        //cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
        // INFO
        /*
        Cursor cursor = db.query(
                FeedEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        */
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        // inserting one row (TABLE_NAME, what if content values is empty, values)
        long newRowId = db.insert(TABLE_NAME, null, contentValues);
        // id=-1 if there was an error
        //id=id of the inserted row

        // This method should return the content URI for the new row. To construct this, append the new row's _ID (or other primary key) value to the table's content URI, using withAppendedId().
        return withAppendedId(uri, newRowId);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        int deletedRows = db.delete(TABLE_NAME, s, strings);
        return deletedRows;
    }

    @Override
    // updates existing record
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        // returns number of rows affected in the db
        int count = db.update(TABLE_NAME, contentValues, null, null);
        return count;
    }
}
