package dam.eet.uvigo.es;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, View.OnClickListener, MapsDialog.MapsDialogListener,
        GoogleMap.OnMarkerClickListener, DeleteMarkerDialog.DeleteMarkerListener {
    private GoogleMap mMap;
    ArrayList<Marker> markerList = new ArrayList<Marker>();

    // variables
    private final double DEFAULT_LAT = 42.169981;
    private final double DEFAULT_LNG = -8.688222;
    LatLng defaultPos = new LatLng(DEFAULT_LAT, DEFAULT_LNG);
    final int REQUEST_LOCATION = 14;

    // FILE
    InternalFile f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // ask for permissions
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        // find button
        Button addmarker_button = findViewById(R.id.addmarker_button);
        addmarker_button.setOnClickListener(this);

        // file
        f = new InternalFile(InternalFile.FILE_NAME_MARKERS);
    }

    @Override
    protected void onStop() {
        buildMarkersString();
        super.onStop();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // zoom buttons and gestures
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        // check permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION);

        } else {
            Log.d("lab6", "set my location enabled");
            mMap.setMyLocationEnabled(true);
        }

        // Add listener
        mMap.setOnMapClickListener(this);
        mMap.setOnMarkerClickListener(this);

        // retrieve files
        retrieveMarkersString();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultPos, 14));
        //cleanMarkersString();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            } else {
                // NICHTS
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.addmarker_button) {
            Marker m;
            LatLng pos = mMap.getCameraPosition().target;
            m = drawMarker(pos);
            showAddMarkerDialog(m);
        }
    }

    private void showAddMarkerDialog(Marker m) {
        Bundle b = new Bundle();
        FragmentManager fm = getSupportFragmentManager();
        MapsDialog editMarkerDialog = new MapsDialog();

        // add info to bundle and show
        b.putString("marker_id", m.getId());
        editMarkerDialog.setArguments(b);
        editMarkerDialog.show(fm, "marker_dialog");

        Log.d("lab6", "marker 1: " + m.getId());
    }

    private void showDeleteMarkerDialog(Marker m) {
        Bundle b = new Bundle();
        FragmentManager fm = getSupportFragmentManager();
        DeleteMarkerDialog deleteMarkerDialog = new DeleteMarkerDialog();

        // add info to bundle and show
        b.putString("marker_id", m.getId());
        deleteMarkerDialog.setArguments(b);
        deleteMarkerDialog.show(fm, "delete_dialog");
    }

    @Override
    public boolean onMarkerClick(Marker m) {
        showDeleteMarkerDialog(m);
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Marker m = drawMarker(latLng);
        showAddMarkerDialog(m);
    }

    private Marker drawMarker(LatLng pos) {
        Marker m = mMap.addMarker(new MarkerOptions().position(pos));
        markerList.add(m);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(m.getPosition()));
        return m;
    }

    @Override
    public void onFinishMapsDialog(String markerID, String inputText) {
        Log.d("lab6", "dialog fini: " + inputText);
        Log.d("lab6", "marker 2: " + markerID);

        Marker m;
        for (int i = 0; i < markerList.size(); i++) {
            m = markerList.get(i);
            if (m.getId().equals(markerID)) {
                if (!inputText.isEmpty()) {
                    m.setTitle(inputText);
                } else {
                    m.hideInfoWindow();
                }
                markerList.set(i, m);
                break;
            }
        }
        /*
        Log.d("lab6", "LISTA: ");
        for (int j = 0; j < markerList.size(); j++) {
            Log.d("lab6", j + " " + markerList.get(j).getTitle());
        }
         */
    }

    @Override
    public void onFinishDeleteMarkerDialog(String markerID) {
        Log.d("lab6", "marker deleted: " + markerID);

        Marker m;
        for (int i = 0; i < markerList.size(); i++) {
            m = markerList.get(i);
            if (m.getId().equals(markerID)) {
                markerList.get(i).remove();
                markerList.remove(i);
                break;
            }
        }
        /*
        Log.d("lab6", "LISTA: ");
        for (int j = 0; j < markerList.size(); j++) {
            Log.d("lab6", j + " " + markerList.get(j).getTitle());
        }
         */
    }

    private StringBuilder buildMarkersString() {
        Log.d("file", "buildMarkersString()");
        StringBuilder sb = new StringBuilder();
        String text;

        for (int i = 0; i < markerList.size(); i++) {
            Marker m = markerList.get(i);
            text = m.getPosition().latitude + "," + m.getPosition().longitude + "," + m.getId();
            //Log.d("file", "text: " + text);
            sb.append(text).append("\n");
        }
        f.saveFile(sb.toString(), this.getApplicationContext());

        return sb;
    }

    private void retrieveMarkersString() {
        String s;
        Boolean eet = false;

        s = f.loadFile(this.getApplicationContext());

        // split
        if (s != null && s != "" && s.length() != 0) {
            String positions[] = s.split("\\r?\\n");
            for (int j = 0; j < positions.length; j++) {
                String[] latlng = positions[j].split(",");
                LatLng x = new LatLng(Double.parseDouble(latlng[0]), Double.parseDouble(latlng[1]));

                Log.d("file", "ll: " + x.toString());
                drawMarker(x);

                // check if if the default marker was included
                if (x.latitude == defaultPos.latitude && x.longitude == defaultPos.longitude) {
                    Log.d("lab7", "default pos!");
                    eet = true;
                }
            }
        }
        if (!eet) {
            drawMarker(defaultPos);
        }
    }

    private void cleanMarkersString() {
        f.saveFile("", this.getApplicationContext());
    }
}